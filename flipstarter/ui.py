# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Instance(object):
    def setupUi(self, Instance):
        Instance.setObjectName("Instance")
        Instance.resize(1054, 519)
        self.gridLayout_4 = QtWidgets.QGridLayout(Instance)
        self.gridLayout_4.setContentsMargins(6, 6, 6, 6)
        self.gridLayout_4.setVerticalSpacing(20)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.mainLayout.setObjectName("mainLayout")
        self.title = QtWidgets.QLabel(Instance)
        font = QtGui.QFont()
        font.setPointSize(36)
        self.title.setFont(font)
        self.title.setObjectName("title")
        self.mainLayout.addWidget(self.title)
        self.subtitle = QtWidgets.QLabel(Instance)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.subtitle.setFont(font)
        self.subtitle.setObjectName("subtitle")
        self.mainLayout.addWidget(self.subtitle)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.templateLayout = QtWidgets.QVBoxLayout()
        self.templateLayout.setObjectName("templateLayout")
        self.template_lbl = QtWidgets.QLabel(Instance)
        self.template_lbl.setObjectName("template_lbl")
        self.templateLayout.addWidget(self.template_lbl)
        self.template_in = QtWidgets.QPlainTextEdit(Instance)
        self.template_in.setTabChangesFocus(True)
        self.template_in.setPlainText("")
        self.template_in.setObjectName("template_in")
        self.templateLayout.addWidget(self.template_in)
        self.metaLayout = QtWidgets.QFormLayout()
        self.metaLayout.setContentsMargins(10, 10, 10, 10)
        self.metaLayout.setObjectName("metaLayout")
        self.pre_amount_lbl = QtWidgets.QLabel(Instance)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.pre_amount_lbl.setFont(font)
        self.pre_amount_lbl.setObjectName("pre_amount_lbl")
        self.metaLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.pre_amount_lbl)
        self.amount_lbl = QtWidgets.QLabel(Instance)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.amount_lbl.setFont(font)
        self.amount_lbl.setObjectName("amount_lbl")
        self.metaLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.amount_lbl)
        self.name_lbl = QtWidgets.QLabel(Instance)
        self.name_lbl.setObjectName("name_lbl")
        self.metaLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.name_lbl)
        self.note_lbl = QtWidgets.QLabel(Instance)
        self.note_lbl.setObjectName("note_lbl")
        self.metaLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.note_lbl)
        self.name = QtWidgets.QLineEdit(Instance)
        self.name.setObjectName("name")
        self.metaLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.name)
        self.note = QtWidgets.QLineEdit(Instance)
        self.note.setPlaceholderText("")
        self.note.setObjectName("note")
        self.metaLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.note)
        self.templateLayout.addLayout(self.metaLayout)
        self.confirm = QtWidgets.QPushButton(Instance)
        self.confirm.setObjectName("confirm")
        self.templateLayout.addWidget(self.confirm)
        self.horizontalLayout.addLayout(self.templateLayout)
        self.arrow = QtWidgets.QLabel(Instance)
        self.arrow.setObjectName("arrow")
        self.horizontalLayout.addWidget(self.arrow)
        self.commitmentLayout = QtWidgets.QVBoxLayout()
        self.commitmentLayout.setObjectName("commitmentLayout")
        self.committx = QtWidgets.QLabel(Instance)
        self.committx.setObjectName("committx")
        self.commitmentLayout.addWidget(self.committx)
        self.commitment = QtWidgets.QPlainTextEdit(Instance)
        self.commitment.setEnabled(False)
        self.commitment.setTabChangesFocus(True)
        self.commitment.setObjectName("commitment")
        self.commitmentLayout.addWidget(self.commitment)
        self.websitehint = QtWidgets.QLabel(Instance)
        self.websitehint.setObjectName("websitehint")
        self.commitmentLayout.addWidget(self.websitehint)
        self.horizontalLayout.addLayout(self.commitmentLayout)
        self.mainLayout.addLayout(self.horizontalLayout)
        self.gridLayout_4.addLayout(self.mainLayout, 0, 0, 1, 1)

        self.retranslateUi(Instance)
        QtCore.QMetaObject.connectSlotsByName(Instance)
        Instance.setTabOrder(self.template_in, self.name)
        Instance.setTabOrder(self.name, self.note)
        Instance.setTabOrder(self.note, self.confirm)
        Instance.setTabOrder(self.confirm, self.commitment)

    def retranslateUi(self, Instance):
        _translate = QtCore.QCoreApplication.translate
        Instance.setWindowTitle(_translate("Instance", "Form"))
        self.title.setText(_translate("Instance", "FlipStarter"))
        self.subtitle.setText(_translate("Instance", "Raise funds cooperatively"))
        self.template_lbl.setText(_translate("Instance", "<html><head/><body><p><span style=\" font-weight:600;\">Project Template</span></p></body></html>"))
        self.pre_amount_lbl.setText(_translate("Instance", "Pledge amount:"))
        self.amount_lbl.setText(_translate("Instance", "0 BCH"))
        self.name_lbl.setText(_translate("Instance", "Your name (optional)"))
        self.note_lbl.setText(_translate("Instance", "Public note (optional)"))
        self.name.setPlaceholderText(_translate("Instance", "Anonymous"))
        self.confirm.setText(_translate("Instance", "Confirm Pledge"))
        self.arrow.setText(_translate("Instance", "<html><head/><body><p><span style=\" font-size:48pt; font-weight:600;\">⇒</span></p></body></html>"))
        self.committx.setText(_translate("Instance", "<html><head/><body><p><span style=\" font-weight:600;\">Commitment Transaction</span></p></body></html>"))
        self.websitehint.setText(_translate("Instance", "<html><head/><body><p align=\"center\">↑ Copy this back to the website  ↑</p></body></html>"))
from . import resources
