import base64
import json
from decimal import Decimal, ROUND_HALF_UP

from electroncash.i18n import _
from electroncash.address import Address
from electroncash.bitcoin import COIN
from electroncash.bitcoin import TYPE_ADDRESS
from electroncash.transaction import Transaction


def decode_template(payload, check_expired = True):
    if len(payload) == 0:
        raise ValueError(_("Empty template."))

    try:
        json_str = base64.b64decode(payload, validate = True)
        tpl = json.loads(json_str)
    except Exception as e:
        raise ValueError("{}: {}".format(_("Failed to decode template"), e))

    if not "output" in tpl:
        raise KeyError(_("Invalid template: Output is missing"))

    if not "address" in tpl["output"]:
        raise KeyError(_("Invalid template: Output is missing address"))

    if not "value" in tpl["output"]:
        raise KeyError(_("Invalid template: Output is missing value"))

    if not "donation" in tpl:
        raise KeyError(_("Invalid template: Donation is missing"))

    if not "amount" in tpl["donation"]:
        raise KeyError(_("Invalid template: Donation is missing amount"))

    if not "expires" in tpl:
        raise KeyError(_("Invalid template: Expiration time missing"))

    if not isinstance(tpl["expires"], int):
        raise ValueError(_("Invalid template: Invalid expiration time"))

    import time
    if check_expired and tpl["expires"] < int(time.time()):
        raise ValueError("Invalid template: Project already expired")

    try:
        tpl["donation"]["amount"] = int(tpl["donation"]["amount"])
    except ValueError as e:
        raise ValueError("Invalid template: Invalid donation amount: {}".format(e))

    if tpl["donation"]["amount"] <= 0:
        raise ValueError("Invalid template: Non-positive donation amount")

    try:
        tpl["output"]["value"] = int(tpl["output"]["value"])
    except ValueError as e:
        raise ValueError("Invalid template: Invalid output value: {}".format(e))

    if tpl["output"]["value"] <= 0:
        raise ValueError("Invalid template: Non-positive output value")

    return tpl

def sats_to_bch(sats):
    satoshi_quantization = Decimal('0.00000000')
    return (Decimal(sats) / COIN).quantize(satoshi_quantization, rounding=ROUND_HALF_UP)

def template_to_tx(tpl, pledge_input):
    addr = Address.from_cashaddr_string(tpl["output"]["address"])
    amount = tpl["output"]["value"]
    out = (TYPE_ADDRESS, addr, amount)

    tx = Transaction.from_io(
            inputs = [pledge_input],
            outputs = [out])
    tx.version = 2
    return tx

def serialize_pledge_input(tx, name, note):
    assert(len(tx.inputs()) == 1)
    txin = tx.inputs()[0]
    assert('signatures' in txin)

    json_str = json.dumps({
        'input': {
            'previout_txid': txin['prevout_hash'],
            'prevout_vout': txin['prevout_n'],
            'sequence': txin['sequence'],
            'scriptsig': tx.input_script(txin, False, tx._sign_schnorr),
        },
        'user': {
            'alias': name,
            'note': note,
        },
    })
    return base64.b64encode(json_str.encode('utf-8')).decode('utf-8')

def debug_trace():
    from PyQt5.QtCore import pyqtRemoveInputHook

    from pdb import set_trace
    pyqtRemoveInputHook()
    set_trace()
