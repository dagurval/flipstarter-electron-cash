import unittest
from .funderutil import decode_template, template_to_tx

class TestFunderUtils(unittest.TestCase):

    def test_decode_template(self):
        payload = 'eyJvdXRwdXQiOnsidmFsdWUiOjAuMDAxLCJhZGRyZXNzIjoiYml0Y29pbmNhc2g6cXF3Z3NyNXVsMG1mdDhlbmR1aHdnbWNrdHI1ODhmZ2s1dW1xdm56OTZmIn0sImRvbmF0aW9uIjp7ImFtb3VudCI6IjAuMDAxMDY4NDQifSwiZXhwaXJlcyI6MTU4MTM3NTU5OX0='

        expected = {
                "output": {
                    "value":100000,
                    "address":"bitcoincash:qqwgsr5ul0mft8enduhwgmcktr588fgk5umqvnz96f",
                    },
                "donation": {
                    "amount": 106844,
                    },
                "expires":1581375599
                }

        res = decode_template(payload, check_expired = False)
        self.assertDictEqual(expected, res)

    def test_sats_to_bch(self):
        self.assertEqual(Decimal('100000.66666668'), sats_to_bch(10000066666668))

    def test_template_to_tx(self):
        tpl = {
                "output": {
                    "value":1012300000,
                    "address":"bitcoincash:pp8skudq3x5hzw8ew7vzsw8tn4k8wxsqsv0lt0mf3g",
                    },
                "donation": {
                    "amount":3333,  # unused in test
                    },
                "expires":1580507999
                }

        dummy_input = {
            'address': "bitcoincash:qpm2qsznhks23z7629mms6s4cwef74vcwvy22gdx6a",
            'type': 'p2pkh',
            'prevout_hash': '7a0e3fcbdaa9ecc6ccce1ad325b6b661e774a57f2e8519c679964e2dd32e200f',
            'prevout_n': 0,
            'value': 1000,
        }

        tx = template_to_tx(tpl, dummy_input)
        self.assertEqual(1, len(tx.inputs()))
        self.assertDictEqual(dummy_input, tx.inputs()[0])
        self.assertEqual(0, tx.locktime)
        self.assertEqual(2, tx.version)
        self.assertEqual(1, len(tx.get_outputs()))

        self.assertEqual(
                'pp8skudq3x5hzw8ew7vzsw8tn4k8wxsqsv0lt0mf3g',
                str(tx.get_outputs()[0][0]))
        self.assertEqual(
                1012300000,
                tx.get_outputs()[0][1])

if __name__ == '__main__':
    unittest.main()
