##!/usr/bin/env python3
#
# Author: Calin Culianu <calin.culianu@gmail.com>
# Author: Dagur Valberg Johannsson <dagurval@pvv.ntnu.no>
# Copyright (C) 2019 Calin Culianu
# Copyright (C) 2020 Dagur Valberg Johannsson
# LICENSE: MIT
#
import sys, os, time, binascii, math

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from electroncash.transaction import InputValueMissing
from electroncash.transaction import Transaction
from electroncash.bitcoin import TYPE_ADDRESS
from electroncash.i18n import _
from electroncash.plugins import BasePlugin, hook
from electroncash.util import NotEnoughFunds
from electroncash.util import print_error
from electroncash.util import PrintError
from electroncash.util import UserCancelled
from electroncash import version

from .funderutil import decode_template
from .funderutil import sats_to_bch
from .funderutil import serialize_pledge_input
from .funderutil import template_to_tx

class Plugin(BasePlugin):

    def __init__(self, parent, config, name):
        super().__init__(parent, config, name)
        # list of 'Instance' objects
        self.instances = list()
        # pointer to Electrum.gui singleton
        self.gui = None
        self.config = config
        self.is_new_network_callback_api = False
        self.is_slp = False
        self.is_shufbeta = False
        self._check_version()  # will set is_new_network_callback_api & is_slp


    def shortName(self):
        return _("FlipStarter")

    def icon(self):
        from .resources import qInitResources # lazy importing
        return QIcon(":flipstarter/resources/icon.png")

    def iconLarge(self):
        from .resources import qInitResources # lazy importing
        return QIcon(":flipstarter/resources/icon64.png")

    def description(self):
        return _("Raise funds cooperatively")

    def thread_jobs(self):
        return list()

    def on_close(self):
        """
        BasePlugin callback called when the wallet is disabled among other things.
        """
        ct = 0
        for instance in self.instances:
            instance.close()
            ct += 1
        self.instances = list()
        self.print_error("on_close: closed %d extant instances" % (ct) )

    @hook
    def init_qt(self, qt_gui):
        """
        Hook called when a plugin is loaded (or enabled).
        """
        self.gui = qt_gui
        # We get this multiple times.  Only handle it once, if unhandled.
        if len(self.instances):
            return

        # These are per-wallet windows.
        for window in self.gui.windows:
            self.load_wallet(window.wallet, window)

    @hook
    def load_wallet(self, wallet, window):
        """
        Hook called when a wallet is loaded and a window opened for it.
        """
        self.instances.append(Instance(self, wallet, window))

    @hook
    def close_wallet(self, wallet):
        for instance in self.instances:
            if instance.wallet == wallet:
                iname = instance.diagnostic_name()
                instance.close()
                # pop it from the list of instances, python gc will remove it
                self.instances.remove(instance)
                self.print_error("removed instance:", iname)
                return

    def _check_version(self):
        full_ver = CopiedCode.parse_package_version(version.PACKAGE_VERSION)
        normalized_ver = full_ver[:-1]
        variant = full_ver[-1]

        if variant == "ShufBeta":
            self.is_shufbeta = True
        elif variant != '':
            self.print_error("Unknown Electron Cash variant:", variant)
            return
        elif normalized_ver >= (3,4) and normalized_ver < (3,5):
            self.is_slp = True

        if self.is_shufbeta:
            min_new_api_ver = (3,9,8)
        elif self.is_slp:
            min_new_api_ver = (3,4,6)
        else:
            min_new_api_ver = (3,3,7)

        self.is_new_network_callback_api = normalized_ver >= min_new_api_ver

class Instance(QWidget, PrintError):
    ''' Encapsulates a wallet-specific instance. '''

    sig_network_updated = pyqtSignal()
    sig_user_tabbed_to_us = pyqtSignal()
    sig_user_tabbed_from_us = pyqtSignal()
    sig_window_moved = pyqtSignal()
    sig_window_resized = pyqtSignal()
    sig_window_activation_changed = pyqtSignal()
    sig_window_unblocked = pyqtSignal()

    def __init__(self, plugin, wallet, window):
        super().__init__()
        self.plugin = plugin
        self.wallet = wallet
        self.window = window
        self.window.installEventFilter(self)
        self.wallet_name = os.path.split(wallet.storage.path)[1]
        self.already_warned_incompatible = False
        self.incompatible, self.is_slp = self.is_wallet_incompatibile()
        self.disabled = False
        self.did_register_callback = False

        # do stuff to setup UI...
        from .ui import Ui_Instance
        self.ui = Ui_Instance()
        self.ui.setupUi(self)

        self.ui.confirm.clicked.connect(self.on_confirm_pledge)

        self.ui.template_in.textChanged.connect(self.on_template_change)
        self.template_stylesheet_ok = self.ui.template_in.styleSheet();

        self.disable_if_incompatible()

        # finally, add the UI to the wallet window
        window.tabs.addTab(self, plugin.icon(), plugin.shortName())

    def get_wallet_password(self):
        password = None
        if self.wallet.has_password():
            password = self.window.password_dialog(
                    _('Your wallet is encrypted.') + '\n' +
                    _('Please enter your password to continue.'))
            if not password:
                raise Exception("Password required")
        return password

    def on_template_change(self):
        self.ui.commitment.setPlainText("")
        self.ui.commitment.setEnabled(False)
        payload = self.ui.template_in.toPlainText()
        if len(payload) == 0:
            self.ui.amount_lbl.setText("0 BCH")
            self.ui.template_in.setStyleSheet(self.template_stylesheet_ok)
            return
        try:
            template = decode_template(payload, check_expired = True)
        except Exception as _:
            self.ui.amount_lbl.setText("UNKNOWN")
            self.ui.template_in.setStyleSheet("background-color: #ffcccb")
            return

        self.ui.template_in.setStyleSheet(self.template_stylesheet_ok)
        donation_bch = sats_to_bch(template["donation"]["amount"])
        self.ui.amount_lbl.setText("{} BCH".format(donation_bch))

    def on_confirm_pledge(self):
        payload = self.ui.template_in.toPlainText()
        try:
            template = decode_template(payload, check_expired = True)
        except Exception as e:
            return self.show_error(str(e))

        donation_sats = template["donation"]["amount"]
        donation_bch_human = sats_to_bch(template["donation"]["amount"])


        if not self.window.question(_("Do you want to pledge to donating {} BCH?").format(donation_bch_human)):
            return self.show_error(_("Pledge cancelled by user"))

        if not self.wallet.network:
            return self.show_error(_("Electron Cash is not connected to the network"))

        try:
            password = self.get_wallet_password()
            tx, pledge_input = self.create_tx_to_self(donation_sats, password)
        except NotEnoughFunds as _e:
            return self.show_error(_("Not enough funds in wallet"))
        except Exception as e:
            e_str = str(e)
            if len(e_str) == 0:
                e_str = "Unknown"
            return self.show_error(_("Failed to create a transaction to ourselves: {}").format(e_str))

        frozen = self.wallet.set_frozen_coin_state([pledge_input], True)

        if frozen != 1:
            return self.show_error(_("Failed to freeze pledge"))

        try:
            signed = self.sign_pledge_tx(password, template, pledge_input)
        except Exception as e:
            return self.show_error(_("Failed to sign pledge: {}").format(e))

        try:
            commitment = serialize_pledge_input(signed,
                    self.ui.name.text(), self.ui.note.text())
        except Exception as e:
            return self.show_error(_("Failed to serialize pledge: {}").format(e))

        if self.wallet.network:
            ok, details = self.wallet.network.broadcast_transaction(tx)
            if not ok:
                return self.show_error(_("Failed to broadcast transaction: {}").format(details))
            self.wallet.set_label(details, _("FlipStarter pledge"))
        else:
            # Should not happen, as we already checked for this above
            # before freezing coins
            return self.show_error(_("Connection error"))

        self.ui.commitment.setPlainText(commitment)
        self.ui.commitment.setEnabled(True)

    def sign_pledge_tx(self, password, template, pledge_input):
        tx = template_to_tx(template, pledge_input)

        # SIGHASH_ANYONECANPAY | SIGHASH_ALL is not supported by
        # electrum, hence this ugly hack.
        signed = False
        tx.__class__ = HackedTransaction
        if self.wallet.is_watching_only():
            raise Exception(_("Watch only wallet"))
        for k in self.wallet.get_keystores():
            try:
                if k.can_sign(tx):
                    k.sign_transaction(tx, password)
                    signed = True
                else:
                    self.print_error("Key store {} cannot sign {}".format(k, tx))
            except UserCancelled:
                continue

        if not signed:
            raise Exception(_("Unable to sign"))

        return tx


    def create_tx_to_self(self, amount, password):
        coins = self.wallet.get_utxos(exclude_frozen=True)

        frozen_pledge_addr = self.wallet.create_new_address()

        tx = self.wallet.make_unsigned_transaction(
                inputs = coins,
                outputs = [(TYPE_ADDRESS, frozen_pledge_addr, amount)],
                config = self.plugin.config)


        self.wallet.sign_transaction(tx, password)

        # find our output pledge output
        pledge_input = None
        for i, (t, addr, v) in enumerate(tx.outputs()):
            if t != TYPE_ADDRESS:
                continue
            if addr != frozen_pledge_addr:
                continue
            if v != amount:
                continue

            # match
            pledge_input = {
                'address': addr,
                'type': 'p2pkh',
                'prevout_hash': tx.txid(),
                'prevout_n': i,
                'sequence': 0xffffffff,
                'value': amount
            }
            self.wallet.add_input_sig_info(pledge_input, addr)
            break

        if pledge_input is None or pledge_input['address'] is None:
            raise Exception(_("Pledge output missing"))

        return tx, pledge_input


    def show_error(self, msg):
        self.window.show_error(msg = msg, title = _("Error: ") + self.plugin.shortName(),
                parent = self.window)

    def on_network(self, event, *args):
        # network thread
        if event == 'updated' and (not args or args[0] is self.wallet):
            self.sig_network_updated.emit() # this passes the call to the gui thread
        elif event == 'verified': # grr.. old api sucks
            self.sig_network_updated.emit() # this passes the call to the gui thread
        elif event in ('wallet_updated', 'verified2') and args[0] is self.wallet:
            self.sig_network_updated.emit() # this passes the call to the gui thread
        elif event == 'blockchain_updated':
            self.sig_network_updated.emit() # this passes the call to the gui thread

    def is_wallet_incompatibile(self):
        is_watching_only_method = getattr(self.wallet, 'is_watching_only', lambda: False)
        is_slp = self.wallet.storage.get('wallet_type', '').strip().lower() == 'bip39-slp'
        try:
            from electroncash.keystore import Hardware_KeyStore
            from electroncash.wallet import Multisig_Wallet
            if (is_slp
                or is_watching_only_method()
                or isinstance(self.wallet, (Multisig_Wallet,))
                or any([isinstance(k, Hardware_KeyStore) for k in self.wallet.get_keystores()])):
                # wallet is multisig, hardware, slp, or watching only.. return True (incompatible)
                return True, is_slp
            else:
                return False, False

        except (ImportError, AttributeError) as e:
            # Hmm. Electron Cash API change? Proceed anyway and the
            # user will just get error messages if plugin can't spend.
            self.print_error("Error checking wallet compatibility:",repr(e))

        return is_watching_only_method() or is_slp, is_slp

    def on_user_tabbed_to_us(self):
        warn_user = False
        try:
            if self.incompatible and not self.already_warned_incompatible:
                self.already_warned_incompatible = True
                warn_user = True
        except AttributeError:
            # Exception happens because I don't think all
            # wallets have the is_watching_only method
            pass

        if warn_user:
            if self.is_slp:
                msg = _("This is an incompatible wallet type.") + "\n\n" \
                + _("SLP token wallets are not supported, as a safety feature.")
            else:
                msg = _("This is an incompatible wallet type.") + "\n\n" \
                + _("The {} plugin only supports imported private key or "
                    "standard spending wallets.").format(self.plugin.shortName())
            self.window.show_error(msg=msg,
                    title=_("{} - Incompatible Wallet").format(self.plugin.shortName()),
                    parent=self.window)

    def disable_if_incompatible(self):
        if self.incompatible:
            self.disabled = True
            self.print_error("Wallet is incompatible, disabling for this wallet")
            gbs = [self.ui.confirm, self.ui.template_in, self.ui.name, self.ui.note]
            for gb in gbs: gb.setEnabled(False) # disable all controls

    def event(self, event):
        '''overrides QObject: a hack used to detect when the prefs
           screen was closed or when our tab comes to foreground. '''
        #self.print_error("got event with type",event.type())
        if event.type() in (QEvent.WindowUnblocked, QEvent.ShowToParent) and self.wallet:
            if event.type() == QEvent.ShowToParent:
                self.sig_user_tabbed_to_us.emit()
            else:
                self.sig_window_unblocked.emit()

            # if window unblocked, maybe user changed prefs.
            # inform our managers to refresh() as maybe base_unit changed, etc.
            self.refresh_all()

        elif event.type() in (QEvent.HideToParent,):
            # user left our UI. Tell interested code about
            # this (mainly the PopupLabel cares)
            self.sig_user_tabbed_from_us.emit()

        # Make real QWidget implementation actually handle the event
        return super().event(event)

    def eventFilter(self, window, event):
        '''Spies on events to parent window to figure out
           when the user moved or resized the window, and announces
           that fact via signals. '''
        #self.print_error("eventFilter got event", event.type())
        if window == self.window:
            if event.type() == QEvent.Move:
                self.sig_window_moved.emit()
            elif event.type() == QEvent.Resize:
                self.sig_window_resized.emit()
            elif event.type() == QEvent.ActivationChange:
                self.sig_window_activation_changed.emit()
        return super().eventFilter(window, event)

    def refresh_all(self):
        self.print_error("refresh_all")

    def wallet_has_password(self):
        try:
            return self.wallet.has_password()
        except AttributeError: # happens on watching-only wallets which don't have the requiside methods
            return False

    # Uncomment to test object lifetime and make sure Qt is deleting us.
    #def __del__(self):
    #    print("**** __del__ ****")

    # called by self.plugin on wallet close - deallocate all resources and die.
    def close(self):
        self.print_error("Close called on an Instance")
        if self.window:
            self.window.removeEventFilter(self)
            ix = self.window.tabs.indexOf(self)
            if ix > -1:
                self.window.tabs.removeTab(ix)
                # since qt doesn't delete us, we need to explicitly
                # delete ourselves, otherwise the QWidget lives around
                # forever in memory
                self.deleteLater()
        self.disabled = True

        # trigger object cleanup sooner rather than later!
        self.window, self.plugin, self.wallet_name = (None,) * 3

    #overrides PrintError super
    def diagnostic_name(self):
        return self.plugin.name + "@" + str(self.wallet_name)


#####

# Hacks to sign with SIGHASH_ANYONECANPAY from jcramer
# https://github.com/simpleledger/Electron-Cash-SLP
class HackedTransaction(Transaction):
    def serialize_preimage(self, i, nHashType=0x00000041, use_cache = False):
        from electroncash.bitcoin import int_to_hex, var_int, bh2u
        if not (nHashType & 0xff) in [0xc1]:
            raise ValueError("wrong hashtype for this hack")

        anyonecanpay = True

        nVersion = int_to_hex(self.version, 4)
        nHashType = int_to_hex(nHashType, 4)
        nLocktime = int_to_hex(self.locktime, 4)

        txin = self.inputs()[i]
        outpoint = self.serialize_outpoint(txin)
        preimage_script = self.get_preimage_script(txin)
        scriptCode = var_int(len(preimage_script) // 2) + preimage_script
        try:
            amount = int_to_hex(txin['value'], 8)
        except KeyError:
            raise InputValueMissing
        nSequence = int_to_hex(txin['sequence'], 4)

        hashPrevouts, hashSequence, hashOutputs = self.calc_common_sighash(use_cache = use_cache)

        hashPrevouts = "0000000000000000000000000000000000000000000000000000000000000000"
        hashSequence = "0000000000000000000000000000000000000000000000000000000000000000"

        preimage = nVersion + hashPrevouts + hashSequence + outpoint + scriptCode + amount + nSequence + bh2u(hashOutputs) + nLocktime + nHashType
        return preimage

    def sign(self, keypairs):
        for i, txin in enumerate(self.inputs()):
            pubkeys, x_pubkeys = self.get_sorted_pubkeys(txin)
            for j, (pubkey, x_pubkey) in enumerate(zip(pubkeys, x_pubkeys)):
                if self.is_txin_complete(txin):
                    # txin is complete
                    break
                if pubkey in keypairs:
                    _pubkey = pubkey
                    kname = 'pubkey'
                elif x_pubkey in keypairs:
                    _pubkey = x_pubkey
                    kname = 'x_pubkey'
                else:
                    continue
                print_error(f"adding signature for input#{i} sig#{j}; {kname}: {_pubkey} schnorr: {self._sign_schnorr}")
                sec, compressed = keypairs.get(_pubkey)
                self._sign_txin(i, j, sec, compressed)
        print_error("is_complete", self.is_complete())
        self.raw = self.serialize()


    def _sign_txin(self, i, j, sec, compressed):
        from electroncash.bitcoin import public_key_from_private_key, Hash, bfh, bh2u
        '''Note: precondition is self._inputs is valid (ie: tx is already deserialized)'''
        pubkey = public_key_from_private_key(sec, compressed)
        # add signature
        nHashType = 0x000000c1
        pre_hash = Hash(bfh(self.serialize_preimage(i, nHashType)))
        if self._sign_schnorr:
            sig = self._schnorr_sign(pubkey, sec, pre_hash)
        else:
            sig = self._ecdsa_sign(sec, pre_hash)
        reason = []
        if not self.verify_signature(bfh(pubkey), sig, pre_hash, reason=reason):
            self.print_error(f"Signature verification failed for input#{i} sig#{j}, reason: {str(reason)}")
            return None
        txin = self._inputs[i]
        txin['signatures'][j] = bh2u(sig + bytes((nHashType & 0xff,)))
        txin['pubkeys'][j] = pubkey # needed for fd keys
        return txin

import re
class CopiedCode:
    ''' Code copied from elsewhere (EC sourcecode, etc), that we can't rely
    on always being there across EC versions.. so we pasted it here. The
    nature of plugins! '''

    _RX_NORMALIZER = re.compile(r'(\.0+)*$')
    _RX_VARIANT_TOKEN_PARSE = re.compile(r'^(\d+)(.+)$')

    @staticmethod
    def normalize_version(v):
        """Used for PROTOCOL_VERSION normalization, e.g '1.4.0' -> (1,4) """
        return tuple(int(x) for x in __class__._RX_NORMALIZER.sub('', v.strip()).split("."))

    @staticmethod
    def parse_package_version(pvstr):
        """ Basically returns a tuple of the normalized version plus the 'variant'
        string at the end. Eg '3.3.0' -> (3, 3, ''), '3.2.2CS' -> (3, 2, 2, 'CS'),
        etc.

        Some more examples:
                '3.3.5CS' -> (3, 3, 5, 'CS')
                '3.4.5_iOS' -> (3, 4, 5, '_iOS')
                '3.3.5' -> (3, 3, 5, '')
                '3.3' -> (3, 3, '')
                '3.3.0' -> (3, 3, '')
                '   3.2.2.0 ILikeSpaces ' -> (3, 2, 2, 'ILikeSpaces')
        Note how 0 fields at the end of the version get normalized with the 0 lopped off:
                '3.3.0' -> (3, 3, '')
                '3.5.0.0.0' -> (3, 5, '')
                '3.5.0.0.0_iOS' -> (3, 5, '_iOS')
        ... and, finally: The last element is *always* going to be present as
        a string, the 'variant'. The 'variant' will be the empty string '' if
        this is the default Electron Cash. If you don't like this heterogeneity of
        types in a tuple, take the retVal[:-1] slice of the array to toss it
        (or just use normalize_version above).
        """
        def raise_(e=None):
            exc = ValueError('Failed to parse package version for: "{}"'.format(pvstr))
            if e: raise exc from e
            else: raise exc
        toks = [x.strip() for x in pvstr.split(".")]
        if not toks:
            raise_()
        if toks[-1].isdigit():
            # Missing 'variant' at end.. add the default '' variant.
            toks.append('')
        else:
            # had 'variant' at end, parse it.
            m = __class__._RX_VARIANT_TOKEN_PARSE.match(toks[-1])
            if m:
                # pop off end and...
                toks[-1:] = [m.group(1), # add the digit portion back (note it's still a str at this point)
                             m.group(2).strip()] # add the leftovers as the actual variant
            else:
                raise_()
        try:
            # make sure everything but the last element is an int.
            toks[:-1] = [int(x) for x in toks[:-1]]
        except ValueError as e:
            raise_(e)
        # .. and.. finally: Normalize it! (lopping off zeros at the end)
        toks[:-1] = __class__.normalize_version('.'.join(str(t) for t in toks[:-1]))
        return tuple(toks)
