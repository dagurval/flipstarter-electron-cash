import sys
import imp
import os
from pathlib import Path

def fix_import():
    try:
        imp.find_module('electroncash')
    except ImportError:
        # Running for unit tests. We need to add Electron-Cash libs
        # as module electroncash. Assumes Electron-Cash is checked out in
        # ~/Electron-Cash
        ec_path = os.getenv("ELECTRON_CASH_HOME",
            os.path.join(Path.home(), "Electron-Cash"))

        ok = False
        if os.path.isdir(ec_path):
            try:
                imp.load_module('electroncash',
                    *imp.find_module(os.path.join(ec_path, 'lib')))
            except Exception as e:
                print("Import failed: {}".format(e), file=sys.stderr)

        if not ok:
            print("Electron-Cash not found at '{}'".format(ec_path), file=sys.stderr)
            print("Use env var ELECTRON_CASH_HOME to change path", file=sys.stderr)
fix_import()

from electroncash.i18n import _

fullname = "FlipStarter"
description = _("Raise funds cooperatively")
available_for = ['qt']
