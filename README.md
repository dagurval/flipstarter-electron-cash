# Flipstarter Electron Cash Plugin

A plugin for Electron Cash that allows you to join crowdsourced funding of projects.

This plugin is licensed under the MIT open source license.

*EXPERMIMENTAL*: Use at own risk.

## Developers

Clone this repo somewhere, then symlink the `flipstarter` folder into `Electron-Cash/plugins`

## Release

Run `zip_n_copy.sh` to generate a zip file that can be installed as an external plugin.

## Tests

The tests assume you have built Electron-Cash in your home dir (~/Electron-Cash/)

To run tests, run
```
python3 -m unittest discover
```

### Building Electron Cash

- cd ~
- `git clone https://github.com/Electron-Cash/Electron-Cash.git`
- Build Electron-Cash (see [README.rst](https://github.com/Electron-Cash/Electron-Cash/blob/master/README.rst))
